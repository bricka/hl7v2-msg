mod err;
mod message;
mod message_iter;
mod mllp;

use std::io::{self, Read, Write};
use std::net::{TcpStream, ToSocketAddrs};
use std::time::Duration;

use getopts::Options;
use log::{debug, info};

use err::FailureCondition;
use mllp::send_message;
use message_iter::MessageIterator;
use openssl::ssl::{SslConnector, SslMethod, SslStream};

trait ReadWrite: Read + Write {}

impl ReadWrite for TcpStream {}
impl ReadWrite for SslStream<TcpStream> {}

struct TlsConfig {
    ca_cert_path: Option<String>,
}

struct Config {
    host: String,
    port: u16,
    tls: Option<TlsConfig>,
}

fn print_usage_and_exit(program_name: &str, opts: &Options, exit_code: i32) {
    let usage = opts.usage(&format!("Usage: {} -H HOSTNAME -p PORT", program_name));
    if exit_code == 0 {
        println!("{}", usage);
    } else {
        eprintln!("{}", usage);
    }

    std::process::exit(exit_code);
}

fn parse_options() -> Result<Config, FailureCondition> {
    let mut opts = Options::new();
    opts.optopt("H", "hostname", "the host to send the message to", "HOST");
    opts.optopt("p", "port", "the port on the host to send the message to", "PORT");
    opts.optflag("", "tls", "use TLS for this connection");
    opts.optopt("", "ca-cert", "a CA cert to trust for TLS", "FILE");
    opts.optflag("h", "help", "display this help");

    let args: Vec<String> = std::env::args().collect();
    let program_name = &args[0];

    let matches = opts.parse(&args[1..])?;

    if matches.opt_present("h") {
        print_usage_and_exit(program_name, &opts, 0);
    }

    if !matches.opt_present("H") {
        eprintln!("No hostname provided!");
        print_usage_and_exit(program_name, &opts, 1);
    }

    if !matches.opt_present("p") {
        eprintln!("No port provided!");
        print_usage_and_exit(program_name, &opts, 1);
    }

    let port_raw_value = matches.opt_str("port").unwrap();

    let tls_config = if matches.opt_present("tls") {
        Some(TlsConfig {
            ca_cert_path: matches.opt_str("ca-cert"),
        })
    } else {
        None
    };

    match port_raw_value.parse::<u16>() {
        Ok(port) => Ok(Config {
            host: matches.opt_str("hostname").unwrap(),
            port,
            tls: tls_config,
        }),
        Err(_) => Err(FailureCondition::ArgumentNotANumber("port".to_string(), port_raw_value))
    }
}

fn connect_to_endpoint(host: &str, port: u16, tls: Option<TlsConfig>) -> Result<Box<dyn ReadWrite>, FailureCondition> {
    let full_host = format!("{}:{}", host, port);
    let addr = full_host.to_socket_addrs()?.next().unwrap();
    let tcp_socket = TcpStream::connect_timeout(&addr, Duration::from_secs(5)).map_err(FailureCondition::from)?;
    tcp_socket.set_read_timeout(Some(Duration::from_secs(5)))?;

    match tls {
        None => Ok(Box::new(tcp_socket)),
        Some(tls_config) => {
            let tls_socket = wrap_connection_in_tls(tcp_socket, host, tls_config)?;
            Ok(Box::new(tls_socket))
        }
    }
}

fn wrap_connection_in_tls(tcp_socket: TcpStream, host: &str, tls_config: TlsConfig) -> Result<SslStream<TcpStream>, FailureCondition> {
    let mut ssl_connector_builder = SslConnector::builder(SslMethod::tls())?;
    if let Some(ca_cert_path) = tls_config.ca_cert_path {
        ssl_connector_builder.set_ca_file(ca_cert_path).unwrap();
    }
    let ssl_connector = ssl_connector_builder.build();

    Ok(ssl_connector.connect(host, tcp_socket)?)
}

fn run() -> Result<(), FailureCondition> {
    let config = parse_options()?;

    info!("Connecting to {}:{}", config.host, config.port);
    let mut socket = connect_to_endpoint(&config.host, config.port, config.tls)?;

    for message in MessageIterator::new(&mut io::stdin()) {
        debug!("About to send message:\n{}", message);
        send_message(&mut socket, &message.get_mllp_string())?;

        match mllp::read_message(&mut socket) {
            Err(e) => {
                debug!("Error while waiting for ACK: {}", e);
                eprintln!("No ACK received");
            },
            Ok(ack) => {
                println!("Received ACK:\n{}", ack);
            }
        }
    }

    Ok(())
}

fn main() {
    env_logger::init();

    let res = run();

    if let Err(e) = res {
        eprintln!("{}", e);
        std::process::exit(1);
    }
}
