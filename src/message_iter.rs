use std::io::{Read, BufRead, BufReader};

use log::error;

use crate::{message::Message, err::FailureCondition};

pub struct MessageIterator<'a> {
    stream: BufReader<&'a mut dyn Read>,
}

impl<'a> MessageIterator<'a> {
    pub fn new(stream: &'a mut dyn Read) -> MessageIterator<'a> {
        MessageIterator {
            stream: BufReader::new(stream),
        }
    }
}

fn convert_buffer_to_message(bytes: Vec<u8>) -> Result<Message, FailureCondition> {
    let string = String::from_utf8(bytes)?;
    Ok(Message::from_human_readable(string))
}

impl<'a> Iterator for MessageIterator<'a> {
    type Item = Message;

    fn next(&mut self) -> Option<Self::Item> {
        let mut accumulator: Vec<u8> = Vec::new();

        loop {
            let mut line = String::new();
            match self.stream.read_line(&mut line) {
                Ok(byte_count) => {
                    if ((line == "\n") || (byte_count == 0)) && !accumulator.is_empty() {
                        // We reached the end of something, and have bytes already.
                        match convert_buffer_to_message(accumulator) {
                            Ok(m) => {
                                return Some(m);
                            },
                            Err(e) => {
                                error!("Received error while converting message: {}", e);
                                return None;
                            }
                        }
                    } else if byte_count == 0 {
                        // EOF, and we have no bytes.
                        return None;
                    } else if line != "\n" {
                        // We read a normal line
                        accumulator.append(&mut line.as_bytes().to_vec());
                    }
                },
                Err(e) => {
                    error!("Received error while reading messages: {}", e);
                    return None;
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn no_message() {
        let mut stream = "".as_bytes();
        let mut iter = MessageIterator::new(&mut stream);
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn one_message() {
        let message_string = "line1\nline2\n";
        let message = Message::from_human_readable(message_string.to_string());

        let mut stream = message_string.as_bytes();

        let mut iter = MessageIterator::new(&mut stream);
        assert_eq!(iter.next(), Some(message));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn two_messages() {
        let message_string_1 = "line1\nline2\n";
        let message_1 = Message::from_human_readable(message_string_1.to_string());

        let message_string_2 = "line3\nline4\n";
        let message_2 = Message::from_human_readable(message_string_2.to_string());

        let mut byte_vec = Vec::new();
        byte_vec.append(&mut message_string_1.as_bytes().to_owned());
        byte_vec.push(b'\n');
        byte_vec.append(&mut message_string_2.as_bytes().to_owned());
        let mut stream = byte_vec.as_slice();

        let mut iter = MessageIterator::new(&mut stream);
        assert_eq!(iter.next(), Some(message_1));
        assert_eq!(iter.next(), Some(message_2));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn ignore_starting_newlines() {
        let message_string_1 = "line1\nline2\n";
        let message_1 = Message::from_human_readable(message_string_1.to_string());

        let message_string_2 = "line3\nline4\n";
        let message_2 = Message::from_human_readable(message_string_2.to_string());

        let mut byte_vec = vec![b'\n', b'\n'];
        byte_vec.append(&mut message_string_1.as_bytes().to_owned());
        byte_vec.push(b'\n');
        byte_vec.push(b'\n');
        byte_vec.push(b'\n');
        byte_vec.append(&mut message_string_2.as_bytes().to_owned());
        let mut stream = byte_vec.as_slice();

        let mut iter = MessageIterator::new(&mut stream);
        assert_eq!(iter.next(), Some(message_1));
        assert_eq!(iter.next(), Some(message_2));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn only_newlines() {
        let byte_vec = vec![b'\n', b'\n'];
        let mut stream = byte_vec.as_slice();

        let mut iter = MessageIterator::new(&mut stream);
        assert_eq!(iter.next(), None);
    }
}
