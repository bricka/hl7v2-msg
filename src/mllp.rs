use std::io::{Write, Read};

use log::{debug, trace};

use crate::{err::FailureCondition, message::Message};

const BLOCK_HEADER: u8 = 0x0B; // Vertical-Tab char, the marker for the start of a message
const BLOCK_FOOTER_1: u8 = 0x1C; // File-Separator Character
const BLOCK_FOOTER_2: u8 = b'\r'; // CR

pub fn send_message(socket: &mut dyn Write, message: &str) -> Result<(), FailureCondition> {
    let message_bytes = message.as_bytes();
    let mut buf = Vec::new();
    buf.write_all(&[BLOCK_HEADER])?;
    buf.write_all(message_bytes)?;
    buf.write_all(&[BLOCK_FOOTER_1, BLOCK_FOOTER_2])?;

    debug!("Will send this buffer (stringified): {:?}", String::from_utf8(buf.to_owned()).unwrap());

    socket.write_all(&buf)?;
    Ok(())
}

#[derive(Debug)]
enum ReadMessageState {
    BeforeHeader,
    InMessage,
    FoundFooter1,
}

pub fn read_message(socket: &mut dyn Read) -> Result<Message, FailureCondition> {
    let mut current_state = ReadMessageState::BeforeHeader;
    let mut buffer: Vec<u8> = Vec::new();
    let mut next_byte_buffer = [0_u8; 1];

    loop {
        trace!("Current state: {:?}", current_state);
        socket.read_exact(&mut next_byte_buffer)?;
        let next_byte = next_byte_buffer[0];

        match current_state {
            ReadMessageState::BeforeHeader => match next_byte {
                BLOCK_HEADER => {
                    current_state = ReadMessageState::InMessage;
                },
                _ => ()
            },
            ReadMessageState::InMessage => match next_byte {
                BLOCK_FOOTER_1 => {
                    current_state = ReadMessageState::FoundFooter1;
                }
                _ => {
                    buffer.push(next_byte);
                }
            },
            ReadMessageState::FoundFooter1 => match next_byte {
                BLOCK_FOOTER_2 => {
                    return String::from_utf8(buffer).map(Message::from_mllp).map_err(FailureCondition::from);
                }
                _ => {
                    buffer.push(BLOCK_FOOTER_1);
                    buffer.push(next_byte);
                    current_state = ReadMessageState::InMessage;
                }
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn send_message_sends_with_envelope() {
        let mut socket = Vec::new();
        let message = "";
        send_message(&mut socket, message).unwrap();
        assert_eq!(socket, vec![BLOCK_HEADER, BLOCK_FOOTER_1, BLOCK_FOOTER_2])
    }

    #[test]
    fn send_message_sends_content() {
        let mut socket = Vec::new();
        let message = "abcd";
        let mut expected = vec![BLOCK_HEADER];
        expected.append(&mut message.as_bytes().to_owned());
        expected.push(BLOCK_FOOTER_1);
        expected.push(BLOCK_FOOTER_2);
        send_message(&mut socket, message).unwrap();
        assert_eq!(socket, expected);
    }

    #[test]
    fn read_message_respects_envelope() {
        let socket = vec![0, 0, BLOCK_HEADER, b'a', b'b', b'c', BLOCK_FOOTER_1, BLOCK_FOOTER_2];

        let actual = read_message(&mut socket.as_slice()).unwrap();
        assert_eq!(actual, Message::from_human_readable("abc".to_string()));
    }

    #[test]
    fn read_message_allows_footer_1_bytes_in_message() {
        let socket = vec![BLOCK_HEADER, b'a', b'b', b'c', BLOCK_FOOTER_1, b'd', BLOCK_FOOTER_1, BLOCK_FOOTER_2];

        let actual = read_message(&mut socket.as_slice()).unwrap();
        assert_eq!(actual, Message::from_human_readable("abc\x1Cd".to_string()));
    }
}
