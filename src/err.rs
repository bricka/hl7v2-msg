use core::fmt;
use std::error::Error;

#[derive(Debug)]
pub enum FailureCondition {
    ArgumentNotANumber(String, String),
    Error(Box<dyn Error>),
    HandshakeWouldBlock,
}

impl fmt::Display for FailureCondition {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            FailureCondition::ArgumentNotANumber(arg, value) => write!(f, "Argument {} is not a number: {}", arg, value),
            FailureCondition::Error(e) => write!(f, "{}", e),
            FailureCondition::HandshakeWouldBlock => write!(f, "Would have blocked while performing TLS handshake"),
        }
    }
}

impl From<getopts::Fail> for FailureCondition {
    fn from(f: getopts::Fail) -> Self {
        FailureCondition::Error(Box::new(f))
    }
}

impl From<std::io::Error> for FailureCondition {
    fn from(e: std::io::Error) -> Self {
        FailureCondition::Error(Box::new(e))
    }
}

impl From<std::net::AddrParseError> for FailureCondition {
    fn from(e: std::net::AddrParseError) -> Self {
        FailureCondition::Error(Box::new(e))
    }
}

impl From<std::string::FromUtf8Error> for FailureCondition {
    fn from(e: std::string::FromUtf8Error) -> Self {
        FailureCondition::Error(Box::new(e))
    }
}

impl<E> From<openssl::ssl::HandshakeError<E>> for FailureCondition {
    fn from(e: openssl::ssl::HandshakeError<E>) -> Self {
        match e {
            openssl::ssl::HandshakeError::SetupFailure(e) => FailureCondition::from(e),
            openssl::ssl::HandshakeError::Failure(s) => FailureCondition::Error(Box::new(s.into_error())),
            openssl::ssl::HandshakeError::WouldBlock(_) => FailureCondition::HandshakeWouldBlock
        }
    }
}

impl From<&openssl::error::Error> for FailureCondition {
    fn from(_e: &openssl::error::Error) -> Self {
        FailureCondition::Error(Box::new(openssl::error::Error::get().unwrap()))
    }
}

impl From<openssl::error::ErrorStack> for FailureCondition {
    fn from(e: openssl::error::ErrorStack) -> Self {
        FailureCondition::Error(Box::new(e))
    }
}
