use std::fmt::Display;

#[derive(Debug, PartialEq)]
pub struct Message {
    human_readable_string: String,
    mllp_string: String,
}

impl Message {
    pub fn from_human_readable(human_readable_string: String) -> Message {
        Message {
            human_readable_string: human_readable_string.clone(),
            mllp_string: human_readable_string.replace('\n', "\r"),
        }
    }

    pub fn from_mllp(mllp_string: String) -> Message {
        Message {
            human_readable_string: mllp_string.replace('\r', "\n"),
            mllp_string,
        }
    }

    pub fn get_human_readable_string(&self) -> String {
        self.human_readable_string.clone()
    }

    pub fn get_mllp_string(&self) -> String {
        self.mllp_string.clone()
    }
}

impl Display for Message {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.get_human_readable_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn starting_with_human_readable_produces_correct_mllp() {
        let human_readable = "line1\nline2\nline3";
        let mllp = "line1\rline2\rline3";
        let message = Message::from_human_readable(human_readable.to_string());
        assert_eq!(message.get_human_readable_string(), human_readable);
        assert_eq!(message.get_mllp_string(), mllp);
    }

    #[test]
    fn starting_with_mllp_produces_correct_human_readable() {
        let human_readable = "line1\nline2\nline3";
        let mllp = "line1\rline2\rline3";
        let message = Message::from_mllp(mllp.to_string());
        assert_eq!(message.get_human_readable_string(), human_readable);
        assert_eq!(message.get_mllp_string(), mllp);
    }
}
