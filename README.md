`hl7v2-msg` is a simple CLI tool to send an HL7v2 message to some endpoint.

# Installation

You will need to have Rust installed: https://www.rust-lang.org/tools/install

Clone this repository and run:
```shell
cargo build --release
```

This will create a binary in the `./target/release` directory. You can then get it into your `PATH` via any of the usual means.

# How to Use

The application provides a `--help` option which explains the parameters. The application reads an HL7 message via STDIN and sends it to the requested endpoint, and waits for an ACK from the server.

At its simplest, you can run:

```shell
hl7v2-msg -H my-endpoint.com -p 3001 < ~/test-message.hl7
```

## TLS

The application supports making TLS connections as well. To do that, you should use the `--tls` option. If you have a custom CA certificate that you would like to trust, you can provide its path with `--ca-cert /path/to/ca/cert.pem`.
